import React, {useState, useEffect} from 'react';
import {View, StyleSheet} from 'react-native';
import {Text, Button, Input, Select, SelectItem} from '@ui-kitten/components';
import {getData, saveData} from '../api';
import _ from 'lodash';

const PlanComponent = ({
  navigation,
  route: {
    params: {plan, setPlan, getPlans},
  },
}) => {
  const [name, setName] = useState('');
  const [exerciseIds, setExerciseIds] = useState([]);
  const [exerciseNames, setExerciseNames] = useState([]);
  const [multiSelectedIndex, setMultiSelectedIndex] = React.useState([]);
  const [exercises, setExercises] = useState([]);

  const getExercises = async () => {
    const localExercises = (await getData('exercises')) || [];
    await setExercises(localExercises);
  };

  useEffect(() => {
    getExercises();
  }, []);

  useEffect(() => {
    const newExercisesIds = [];
    const newExercisesNames = [];

    if (multiSelectedIndex?.length) {
      multiSelectedIndex.forEach(el => {
        newExercisesIds.push(exercises[el.row].id);
        newExercisesNames.push(exercises[el.row].name);
      });
    }
    setExerciseIds(newExercisesIds);
    setExerciseNames(newExercisesNames);
  }, [multiSelectedIndex, exercises]);

  useEffect(() => {
    if (plan && exercises?.length) {
      setName(plan.name);
      setMultiSelectedIndex(plan.exercisesIndexes);
      setExerciseNames(plan.exerciseNames);
    }
  }, [plan, exercises]);

  const savePlan = async () => {
    const plans = (await getData('plans')) || [];
    if (plan) {
      const localPlan = await _.find(plans, {id: plan.id});
      localPlan.name = await name;
      localPlan.exercises = await exerciseIds;
      localPlan.exercisesIndexes = await multiSelectedIndex;
    } else {
      await plans.push({
        id: Math.random(),
        name: name,
        exercisesIndexes: multiSelectedIndex,
        exercises: exerciseIds,
      });
    }
    await saveData('plans', plans);
    await setPlan(null);
    await getPlans();
    await navigation.pop();
  };

  const deletePlan = async () => {
    let plans = (await getData('plans')) || [];
    plans = plans.filter(el => el.id !== plan.id);
    await saveData('plans', plans);
    await setPlan(null);
    await getPlans();
    await navigation.pop();
  };

  return (
    <View style={styles.mainView}>
      {plan ? (
        <Text category="h5">Edytuj plan treningowy</Text>
      ) : (
        <Text category="h5">Nowy plan treningowy</Text>
      )}
      <Input
        value={name}
        onChangeText={nextValue => setName(nextValue)}
        placeholder="Nazwa planu"
      />
      <Select
        multiSelect={true}
        value={exerciseNames?.length ? exerciseNames.join(', ') : ''}
        selectedIndex={multiSelectedIndex}
        placeholder="Wybierz ćwiczenia"
        onSelect={index => setMultiSelectedIndex(index)}>
        {exercises.map(ex => (
          <SelectItem title={ex.name} />
        ))}
      </Select>
      <Button onPress={() => savePlan()} disabled={!name} status="success">
        Zapisz
      </Button>
      {plan ? (
        <Button onPress={() => deletePlan()} status="danger">
          Usuń
        </Button>
      ) : (
        <View />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  mainView: {
    justifyContent: 'space-around',
    height: '100%',
    padding: 40,
  },
});

export default PlanComponent;
