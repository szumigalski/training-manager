import React, {useState, useEffect} from 'react';
import {View, ScrollView, StyleSheet} from 'react-native';
import {
  Text,
  Button,
  TabBar,
  Tab,
  Calendar,
  NativeDateService,
} from '@ui-kitten/components';
import {getData, saveData} from '../api';
import {cloneDeep, find} from 'lodash';
import moment from 'moment';
import {colorPalette} from '../config';

const TrainingHistoryComponent = () => {
  const [trainings, setTrainings] = useState([]);
  const [plans, setPlans] = useState([]);
  const [historyType, setHistoryType] = useState(1);

  const getTrainings = async () => {
    const localTrainings = (await getData('trainings')) || [];
    const localPlans = (await getData('plans')) || [];
    await setTrainings(localTrainings);
    await setPlans(localPlans);
  };

  useEffect(() => {
    getTrainings();
  }, []);

  const deleteTraining = async id => {
    let localTrainings = await cloneDeep(trainings);
    localTrainings = await localTrainings.filter(tr => tr.id !== id);
    let exers = (await getData('exercises')) || [];
    (await exers?.length) &&
      exers.map(exercise => {
        exercise.lastTraining = exercise.lastTraining.filter(tr => tr !== id);
      });
    await saveData('exercises', exers);
    await saveData('trainings', localTrainings);
    await setTrainings(localTrainings);
  };

  const DayCell = ({date}, style) => (
    <View
      style={[
        styles.dayContainer,
        style.container,
        find(trainings, {date: moment(date).format('DD-MM-YYYY')})
          ? styles.green
          : styles.red,
      ]}>
      <Text style={style.text}>{`${date.getDate()}`}</Text>
    </View>
  );
  const i18n = {
    dayNames: {
      short: ['Nie', 'Pon', 'Wt', 'Śr', 'Czw', 'Pi', 'Sob'],
      long: [
        'Niedziela',
        'Poniedziałek',
        'Wtorek',
        'Środa',
        'Czwartek',
        'Piątek',
        'Sobota',
      ],
    },
    monthNames: {
      short: [
        'St',
        'Lut',
        'Mar',
        'Kwi',
        'Maj',
        'Cze',
        'Li',
        'Sie',
        'Wrz',
        'Paź',
        'Lis',
        'Gru',
      ],
      long: [
        'Styczeń',
        'Luty',
        'Marzec',
        'Kwiecień',
        'Maj',
        'Czerwiec',
        'Lipiec',
        'Sierpień',
        'Wrzesiweń',
        'Październik',
        'Listopad',
        'Grudzień',
      ],
    },
  };
  const localeDateService = new NativeDateService('pl', {
    i18n,
    startDayOfWeek: 1,
  });

  return (
    <ScrollView style={styles.mainScroll}>
      <Text category="h5" style={styles.mainHeader}>
        Historia treningów
      </Text>
      <TabBar
        selectedIndex={historyType}
        style={{
          backgroundColor: colorPalette.backgroundColor,
          color: colorPalette.textColor,
        }}
        onSelect={index => setHistoryType(index)}>
        <Tab title="Lista" />
        <Tab title="Kalendarz" />
      </TabBar>
      {historyType === 0 && trainings.length ? (
        trainings.map(training => (
          <View key={training.id} style={styles.trainingItem}>
            <Text>{`${training.date} - ${
              find(plans, {
                id: training.planId,
              })?.name
            }`}</Text>
            <Button onPress={() => deleteTraining(training.id)} status="danger">
              <Text>Usuń</Text>
            </Button>
          </View>
        ))
      ) : (
        <View />
      )}
      {historyType === 1 && trainings.length ? (
        <View>
          <Calendar renderDay={DayCell} dateService={localeDateService} />
        </View>
      ) : (
        <View />
      )}
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  dayContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    aspectRatio: 1,
  },
  value: {
    fontSize: 12,
    fontWeight: '400',
  },
  green: {
    backgroundColor: 'green',
  },
  red: {
    backgroundColor: 'red',
  },
  mainScroll: {padding: 20, backgroundColor: colorPalette.backgroundColor},
  mainHeader: {color: colorPalette.textColor, marginBottom: 20},
  trainingItem: {
    padding: 15,
    margin: 5,
    backgroundColor: colorPalette.backgroundColor,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderRadius: 8,
  },
});
export default TrainingHistoryComponent;
