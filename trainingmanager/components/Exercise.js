import React, {useState, useEffect} from 'react';
import {View, StyleSheet} from 'react-native';
import {Text, Button, Input} from '@ui-kitten/components';
import {getData, saveData} from '../api';
import _ from 'lodash';
import {colorPalette} from '../config';

const ExerciseComponent = ({
  navigation,
  route: {
    params: {exercise, setExercise, getExercises},
  },
}) => {
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');

  useEffect(() => {
    if (exercise) {
      setName(exercise.name);
      setDescription(exercise.description);
    }
  }, [exercise]);

  const saveExercise = async () => {
    const exercises = (await getData('exercises')) || [];
    if (exercise) {
      const localExercise = await _.find(exercises, {id: exercise.id});
      localExercise.name = await name;
      localExercise.description = await description;
    } else {
      await exercises.push({
        id: Math.random(),
        name: name,
        description: description,
        lastTraining: [],
      });
    }
    await saveData('exercises', exercises);
    await setExercise(null);
    await getExercises();
    await navigation.pop();
  };

  const deleteExercise = async () => {
    let exercises = (await getData('exercises')) || [];
    let plans = (await getData('plans')) || [];
    exercises = exercises.filter(el => el.id !== exercise.id);
    plans?.length &&
      plans.map(plan => {
        plan.exercises = plan.exercises.filter(ex => ex.id !== exercise.id);
      });
    await saveData('plans', plans);
    await saveData('exercises', exercises);
    await setExercise(null);
    await navigation.pop();
  };

  return (
    <View style={styles.container}>
      {exercise ? (
        <Text style={{color: colorPalette.textColor}} category="h5">
          Edytuj ćwiczenie
        </Text>
      ) : (
        <Text style={{color: colorPalette.textColor}} category="h5">
          Nowe ćwiczenie
        </Text>
      )}
      <Input
        value={name}
        onChangeText={nextValue => setName(nextValue)}
        placeholder="Nazwa ćwiczenia"
      />
      <Input
        value={description}
        onChangeText={nextValue => setDescription(nextValue)}
        placeholder="opis ćwiczenia"
      />
      <Button onPress={() => saveExercise()} disabled={!name} status="success">
        Zapisz
      </Button>
      {exercise ? (
        <Button onPress={() => deleteExercise()} status="danger">
          Usuń
        </Button>
      ) : (
        <View />
      )}
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    justifyContent: 'space-around',
    height: '100%',
    padding: 40,
    backgroundColor: colorPalette.backgroundColor,
  },
});
export default ExerciseComponent;
