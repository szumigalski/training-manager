import React, {useState, useEffect} from 'react';
import {View, StyleSheet} from 'react-native';
import {Text, Button, Select, SelectItem, Divider} from '@ui-kitten/components';
import {getData} from '../api';
import {colorPalette} from '../config';

const EditExercisesPlansComponent = ({navigation}) => {
  const [exercises, setExercises] = useState([]);
  const [exercise, setExercise] = useState(null);
  const [plan, setPlan] = useState(null);
  const [plans, setPlans] = useState([]);

  const getExercises = async () => {
    const localExercises = await getData('exercises');
    await setExercises(localExercises);
  };

  const getPlans = async () => {
    const localPlans = await getData('plans');
    await setPlans(localPlans);
  };

  useEffect(() => {
    getExercises();
    getPlans();
  }, [exercise]);

  return (
    <View style={styles.container}>
      <Text style={{color: colorPalette.textColor}} category="h5">
        Edycja ćwiczeń / planów
      </Text>
      <Button
        status="success"
        onPress={() =>
          navigation.navigate('Exercise', {
            exercise: null,
            setExercise: setExercise,
            getExercises: getExercises,
          })
        }>
        Nowe ćwiczenie
      </Button>
      <View>
        <Button
          onPress={() =>
            navigation.navigate('Exercise', {
              exercise: exercise,
              setExercise: setExercise,
            })
          }
          disabled={!exercise}>
          Edytuj ćwiczenie
        </Button>
        {exercises?.length ? (
          <Select
            onSelect={ex => setExercise(exercises[ex.row])}
            value={exercise ? exercise.name : ''}>
            {exercises.map(ex => (
              <SelectItem title={ex.name} key={ex.id} />
            ))}
          </Select>
        ) : (
          <View />
        )}
      </View>
      <Divider />
      <Button
        status="success"
        onPress={() =>
          navigation.navigate('Plan', {
            plan: null,
            setPlan: setPlan,
            getPlans: getPlans,
          })
        }>
        Nowy plan
      </Button>
      <View>
        <Button
          onPress={() =>
            navigation.navigate('Plan', {
              plan: plan,
              setPlan: setPlan,
              getPlans: getPlans,
            })
          }
          disabled={!plan}>
          Edytuj plan
        </Button>
        {plans?.length ? (
          <Select
            onSelect={pl => setPlan(plans[pl.row])}
            value={plan ? plan.name : ''}>
            {plans.map(pl => (
              <SelectItem title={pl.name} key={pl.id} />
            ))}
          </Select>
        ) : (
          <View />
        )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: 'space-around',
    height: '100%',
    padding: 40,
    backgroundColor: colorPalette.backgroundColor,
  },
});

export default EditExercisesPlansComponent;
