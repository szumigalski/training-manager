import React, {useState, useEffect} from 'react';
import {View, StyleSheet} from 'react-native';
import {Input, Text, Button, Icon, Divider} from '@ui-kitten/components';
import {cloneDeep, find} from 'lodash';
import {getData, saveData} from '../api';

const TrainingExercise = ({
  navigation,
  route: {
    params: {exercise, updateTraining, trainingId},
  },
}) => {
  const [localExercise, setLocalExercise] = useState(null);
  const [exerciseHistory, setExerciseHistory] = useState(null);
  const [historyCounter, setHistoryCounter] = useState(0);

  useEffect(() => {
    setLocalExercise(exercise);
  }, [exercise]);

  const saveExercisesHistory = async () => {
    const localExercises = await getData('exercises');
    const trainings = await getData('trainings');
    const localEx = await find(localExercises, {id: localExercise.id})
      .lastTraining;
    const fullHistory = [];
    await localEx.forEach(trainId => {
      const localTrain = find(trainings, {id: trainId});
      const trainEx = find(localTrain.exercises, {id: localExercise.id});
      fullHistory.push(trainEx.cycleList);
    });
    await setExerciseHistory(fullHistory);
    fullHistory.length && (await setHistoryCounter(fullHistory.length - 1));
  };

  useEffect(() => {
    localExercise && saveExercisesHistory();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [localExercise]);

  const AddIcon = props => <Icon name="plus-outline" {...props} />;

  const addExerciseToHistory = async () => {
    const localExercises = await getData('exercises');
    const localEx = await find(localExercises, {id: localExercise.id});
    if (!localEx.lastTraining.length) {
      await localEx.lastTraining.push(trainingId);
    } else {
      if (
        !(localEx.lastTraining[localEx.lastTraining.length - 1] === trainingId)
      ) {
        await localEx.lastTraining.push(trainingId);
      }
    }
    await saveData('exercises', localExercises);
  };

  const ArrowLeftIcon = props => (
    <Icon name="arrow-circle-left-outline" {...props} />
  );

  const ArrowRightIcon = props => (
    <Icon name="arrow-circle-right-outline" {...props} />
  );
  return (
    <View>
      <View style={styles.mainView}>
        <Text style={styles.exerciseName}>{localExercise?.name}</Text>
        <Text>Waga / Powtórzenia</Text>
      </View>
      <Divider style={styles.divider} />
      <View style={styles.leftPanel}>
        <View style={styles.leftPanelWidth}>
          {exerciseHistory && exerciseHistory[historyCounter] ? (
            exerciseHistory[historyCounter].map((el, i) => (
              <View key={i}>
                <Text style={styles.textPadding}>
                  {(el?.weight).toString()} / {(el?.count).toString()}
                </Text>
                <Divider style={styles.divider} />
              </View>
            ))
          ) : (
            <View />
          )}
          <View style={styles.rigthPanel}>
            <Button
              size="large"
              disabled={historyCounter === 0}
              style={styles.rightButton}
              onPress={() => setHistoryCounter(historyCounter - 1)}
              accessoryLeft={ArrowLeftIcon}
            />
            <Button
              size="large"
              disabled={
                historyCounter === exerciseHistory?.length - 1 ||
                !exerciseHistory?.length
              }
              style={styles.rightButton}
              onPress={() => setHistoryCounter(historyCounter + 1)}
              accessoryLeft={ArrowRightIcon}
            />
          </View>
        </View>
        <View style={styles.rightPanelWidth}>
          {localExercise &&
            localExercise?.cycleList.map((cycle, i) => (
              <View key={cycle.number}>
                <View style={styles.rigthPanelCycle}>
                  <Input
                    value={cycle.weight.toString()}
                    type="number"
                    onChangeText={nextValue => {
                      const newExercise = cloneDeep(localExercise);
                      newExercise.cycleList[i].weight = nextValue;
                      setLocalExercise(newExercise);
                    }}
                    placeholder="kg"
                  />
                  <Text>/</Text>
                  <Input
                    value={cycle.count.toString()}
                    type="number"
                    onChangeText={nextValue => {
                      const newExercise = cloneDeep(localExercise);
                      newExercise.cycleList[i].count = nextValue;
                      setLocalExercise(newExercise);
                    }}
                    placeholder="c"
                  />
                </View>
                <Divider style={styles.divider} />
              </View>
            ))}
          <Button
            size="large"
            onPress={() => {
              const newExercise = cloneDeep(localExercise);
              newExercise.cycleList.push({
                weight: 0,
                serieNumber: localExercise.cycleList.length + 1,
                count: 0,
              });
              setLocalExercise(newExercise);
            }}
            style={styles.newCycle}
            accessoryLeft={AddIcon}
            status="success"
          />
        </View>
      </View>
      <Divider style={styles.dividerMargin} />
      <View style={styles.exit}>
        <Button onPress={() => navigation.pop()} status="danger">
          Wyjdź
        </Button>
        <Button
          status="success"
          onPress={async () => {
            updateTraining(localExercise);
            addExerciseToHistory();
          }}>
          Zapisz
        </Button>
        <Button
          status="success"
          onPress={() => {
            updateTraining(localExercise);
            addExerciseToHistory();
            navigation.pop();
          }}>
          Zapisz i wyjdź
        </Button>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  mainView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  exerciseName: {fontSize: 20, padding: 10},
  divider: {
    backgroundColor: 'black',
  },
  leftPanel: {flexDirection: 'row', height: '82%'},
  leftPanelWidth: {
    width: '55%',
  },
  textPadding: {padding: 19},
  rigthPanel: {
    flexDirection: 'row',
    position: 'absolute',
    bottom: 0,
    width: '100%',
    justifyContent: 'space-around',
  },
  rightButton: {
    width: 80,
  },
  rightPanelWidth: {width: '45%', padding: 4},
  rigthPanelCycle: {flexDirection: 'row', padding: 10},
  newCycle: {
    height: 50,
    position: 'absolute',
    bottom: 4,
    width: '100%',
    left: 4,
  },
  exit: {
    flexDirection: 'row',
    height: 50,
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  dividerMargin: {backgroundColor: 'black', marginTop: 10},
});
export default TrainingExercise;
