import React, {useEffect, useState} from 'react';
import {View, StyleSheet, ScrollView} from 'react-native';
import {CheckBox, Text, Button, Icon, Modal, Card} from '@ui-kitten/components';
import {getData, saveData} from '../api';
import {cloneDeep, findIndex, find} from 'lodash';
import {colorPalette} from '../config';

const TrainingComponent = ({
  navigation,
  route: {
    params: {trainingId},
  },
}) => {
  const [visible, setVisible] = useState(false);
  const [trainings, setTrainings] = useState([]);
  const [localTraining, setLocalTraining] = useState();

  const getTrainings = async () => {
    const localTrainings = (await getData('trainings')) || [];
    await setTrainings(localTrainings);
  };

  useEffect(() => {
    getTrainings();
  }, []);

  useEffect(() => {
    if (trainings?.length && trainingId && find(trainings, {id: trainingId})) {
      setLocalTraining(find(trainings, {id: trainingId}));
    }
  }, [trainings, trainingId]);

  const InfoIcon = props => <Icon name="info-outline" {...props} />;

  const ArrowRightIcon = props => (
    <Icon name="arrow-circle-right-outline" {...props} />
  );

  const updateTraining = newExercise => {
    const updatedTraining = cloneDeep(localTraining);
    const indexExercise = findIndex(updatedTraining.exercises, {
      id: newExercise.id,
    });
    updatedTraining.exercises.splice(indexExercise, 1, newExercise);
    const indexTraining = findIndex(trainings, {id: trainingId});
    trainings.splice(indexTraining, 1, updatedTraining);
    saveData('trainings', trainings);
    setTrainings(trainings);
    setLocalTraining(updatedTraining);
  };

  const checkExercise = exerciseId => {
    const updatedTraining = cloneDeep(localTraining);
    const indexExercise = findIndex(updatedTraining.exercises, {
      id: exerciseId,
    });
    const exercise = find(updatedTraining.exercises, {id: exerciseId});
    exercise.isDone = !exercise.isDone;
    updatedTraining.exercises.splice(indexExercise, 1, exercise);
    const indexTraining = findIndex(trainings, {id: trainingId});
    trainings.splice(indexTraining, 1, updatedTraining);
    saveData('trainings', trainings);
    setTrainings(trainings);
    setLocalTraining(updatedTraining);
  };

  return (
    <View style={styles.mainView}>
      <Text style={{color: colorPalette.textColor}} category="h5">
        Trening
      </Text>
      <ScrollView style={styles.mainScroll}>
        {localTraining?.exercises?.length &&
          localTraining.exercises.map(exercise => (
            <View key={exercise.id} style={styles.exerciseItem}>
              <View style={styles.exerciseItemLeft}>
                <CheckBox
                  checked={exercise.isDone}
                  onChange={() => checkExercise(exercise.id)}
                />
                <Text style={styles.exerciseNameText}>{exercise.name}</Text>
              </View>
              <View style={styles.exerciseItemRight}>
                <Button
                  size="large"
                  onPress={() => setVisible(exercise.description)}
                  appearance="outline"
                  status="primary"
                  style={styles.exerciseDescriptionText}
                  accessoryLeft={InfoIcon}
                />
                <Button
                  size="large"
                  appearance="outline"
                  status="success"
                  onPress={() =>
                    navigation.navigate('TrainingExercise', {
                      exercise: exercise,
                      updateTraining: updateTraining,
                      trainingId: localTraining.id,
                    })
                  }
                  style={styles.arrowRight}
                  accessoryLeft={ArrowRightIcon}
                />
              </View>
            </View>
          ))}
      </ScrollView>
      <Button
        onPress={() => navigation.pop()}
        style={{
          backgroundColor: colorPalette.successColor,
          borderColor: colorPalette.successColor,
        }}>
        Zakończ trening
      </Button>
      <Modal
        visible={visible}
        backdropStyle={styles.backdrop}
        onBackdropPress={() => setVisible(false)}>
        <Card disabled={true}>
          <Text>Opis ćwiczenia:</Text>
          <Text>{visible}</Text>
        </Card>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  mainView: {
    padding: 20,
    backgroundColor: colorPalette.backgroundColor,
  },
  exerciseItem: {
    padding: 15,
    margin: 5,
    flexDirection: 'row',
    borderColor: colorPalette.textColor,
    borderWidth: 1,
    justifyContent: 'space-between',
    borderRadius: 8,
  },
  exerciseItemLeft: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  exerciseNameText: {
    marginLeft: 10,
    color: colorPalette.textColor,
    width: 120,
  },
  exerciseItemRight: {
    flexDirection: 'row',
  },
  exerciseDescriptionText: {
    width: 70,
    marginRight: 10,
  },
  arrowRight: {
    width: 70,
  },
  mainScroll: {
    height: '90%',
  },
});

export default TrainingComponent;
