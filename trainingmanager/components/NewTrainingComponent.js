import React, {useEffect, useState} from 'react';
import {ScrollView, View, StyleSheet} from 'react-native';
import {Text} from '@ui-kitten/components';
import {cloneDeep, find} from 'lodash';
import {Button, Icon, Radio} from '@ui-kitten/components';
import {getData, saveData} from '../api';
import moment from 'moment';
import {colorPalette} from '../config';

const NewTrainingComponent = ({navigation}) => {
  const [plans, setPlans] = useState([]);
  const [selectedItem, setSelectedItem] = useState(null);
  const [selectedPlan, setSelectedPlan] = useState(null);
  const [trainings, setTrainings] = useState(null);

  const ArrowUpIcon = props => (
    <Icon name="arrow-ios-upward-outline" {...props} />
  );

  const ArrowDownIcon = props => (
    <Icon name="arrow-ios-downward-outline" {...props} />
  );

  const getPlans = async () => {
    const localExercices = await getData('exercises');
    const localPlans = (await getData('plans')) || [];
    (await localPlans.length) &&
      localPlans.map((plan, i) => {
        plan.fullExercises = [];
        plan.exercises.map(exercise => {
          const newExercise = cloneDeep(find(localExercices, {id: exercise}));
          plan.fullExercises.push(newExercise);
        });
      });
    await setPlans(localPlans);
  };

  const getTrainings = async () => {
    const localTrainings = (await getData('trainings')) || [];
    await setTrainings(localTrainings);
  };

  useEffect(() => {
    getPlans();
    getTrainings();
  }, []);

  const createTraining = () => {
    const newExercises = [];
    find(plans, {id: selectedPlan}).fullExercises.map(ex => {
      newExercises.push({
        id: ex.id,
        name: ex.name,
        description: ex.description,
        lastTraining: ex.lastTraining,
        isDone: false,
        cycleList: [
          {
            weight: 0,
            serieNumber: 1,
            count: 0,
          },
        ],
      });
    });
    const newTraining = {
      id: Math.random(),
      date: moment().format('DD-MM-YYYY'),
      planId: selectedPlan,
      exercises: newExercises,
    };
    trainings.push(newTraining);
    saveData('trainings', trainings);
    navigation.navigate('Training', {
      trainingId: newTraining.id,
    });
  };

  return (
    <View style={styles.mainView}>
      <Text style={{color: colorPalette.textColor}} category="h5">
        NOWY TRENINIG
      </Text>
      <ScrollView style={styles.mainScrollView}>
        {plans?.length ? (
          plans.map(plan => (
            <View key={plan.id}>
              <View style={styles.planItem}>
                <View style={styles.planItemView}>
                  <Radio
                    style={styles.planItemCheckBox}
                    checked={selectedPlan === plan.id}
                    onChange={() => setSelectedPlan(plan.id)}
                  />
                  <Text>{plan.name}</Text>
                </View>
                <Button
                  onPress={() =>
                    setSelectedItem(selectedItem === plan.id ? null : plan.id)
                  }
                  style={styles.planItemButton}
                  accessoryLeft={
                    selectedItem === plan.id ? ArrowUpIcon : ArrowDownIcon
                  }
                />
              </View>
              {selectedItem === plan.id &&
                plan.fullExercises.map(ex => (
                  <View key={ex.id} style={styles.exerciseName}>
                    <Text>{ex.name}</Text>
                  </View>
                ))}
            </View>
          ))
        ) : (
          <View />
        )}
      </ScrollView>
      <Button
        disabled={!selectedPlan}
        status="success"
        onPress={() => createTraining()}
        style={{
          borderColor: colorPalette.successColor,
        }}>
        Rozpocznij trening
      </Button>
    </View>
  );
};
const styles = StyleSheet.create({
  mainView: {
    padding: 20,
    backgroundColor: colorPalette.backgroundColor,
  },
  mainScrollView: {
    height: '85%',
    backgroundColor: colorPalette.textColor,
    margin: 10,
    padding: 10,
    borderRadius: 5,
  },
  planItem: {
    paddingLeft: 30,
    paddingRight: 30,
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: colorPalette.backgroundColor,
    borderRadius: 5,
    margin: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
  },
  planItemView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  planItemCheckBox: {
    marginRight: 5,
    backgroundColor: 'white',
    borderRadius: 50,
  },
  planItemButton: {
    width: 50,
    backgroundColor: colorPalette.textColor,
    borderColor: colorPalette.textColor,
  },
  exerciseName: {
    padding: 15,
    margin: 5,
    marginLeft: 50,
    backgroundColor: colorPalette.backgroundColor,
    borderRadius: 5,
  },
});
export default NewTrainingComponent;
