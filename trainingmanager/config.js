export const colorPalette = {
  backgroundColor: '#eee',
  textColor: '#2C4251',
  successColor: '#5E8C61',
  failColor: '#D16666',
  infoColor: '#95B8D1',
  backgroundSecondColor: '#D1D0A3',
};
