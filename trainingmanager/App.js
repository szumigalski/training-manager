import React from 'react';
import {StyleSheet, View, Image} from 'react-native';
import * as eva from '@eva-design/eva';
import {ApplicationProvider, Button} from '@ui-kitten/components';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import EditExercisesPlansComponent from './components/EditExercisesPlans';
import TrainingHistoryComponent from './components/TrainingHistory';
import StatisticsComponent from './components/Statistics';
import NewTrainingComponent from './components/NewTrainingComponent';
import ExerciseComponent from './components/Exercise';
import PlanComponent from './components/Plan';
import TrainingComponent from './components/TrainingComponent';
import TrainingExerciseComponent from './components/TrainingExercise';
import {IconRegistry} from '@ui-kitten/components';
import {EvaIconsPack} from '@ui-kitten/eva-icons';
import Toast from 'react-native-toast-message';
import Logo from './assets/mainLogo.png';
import {colorPalette} from './config';
import theme from './components/theme.json';

const Stack = createNativeStackNavigator();
const HomeComponent = ({navigation}) => {
  return (
    <View style={styles.centralView}>
      <Image source={Logo} style={styles.mainImage} resizeMode="contain" />
      <View style={styles.mainView}>
        <Button
          onPress={() => navigation.navigate('New')}
          status="primary"
          appearance="outline"
          style={styles.mainButton}>
          NOWY TRENING
        </Button>
        <Button
          onPress={() => navigation.navigate('History')}
          status="primary"
          appearance="outline"
          style={styles.mainButton}>
          HISTORIA TRENINGÓW
        </Button>
        <Button
          onPress={() => navigation.navigate('Statistics')}
          status="primary"
          style={styles.mainButton}
          appearance="outline">
          STATYSTYKI
        </Button>
        <Button
          onPress={() => navigation.navigate('Edit')}
          status="primary"
          appearance="outline"
          style={styles.mainButton}>
          EDYCJA ĆWICZEŃ/PLANÓW
        </Button>
      </View>
    </View>
  );
};

const App = () => {
  return (
    <>
      <NavigationContainer>
        <IconRegistry icons={EvaIconsPack} />
        <ApplicationProvider {...eva} theme={theme}>
          <Stack.Navigator
            initialRouteName="Home"
            screenOptions={{
              headerShown: false,
            }}>
            <Stack.Screen
              name="Home"
              component={HomeComponent}
              screenOptions={{
                headerShown: false,
              }}
            />
            <Stack.Screen
              name="Edit"
              component={EditExercisesPlansComponent}
              screenOptions={{
                headerShown: false,
              }}
            />
            <Stack.Screen
              name="History"
              component={TrainingHistoryComponent}
              screenOptions={{
                headerShown: false,
              }}
            />
            <Stack.Screen
              name="Statistics"
              component={StatisticsComponent}
              screenOptions={{
                headerShown: false,
              }}
            />
            <Stack.Screen
              name="New"
              component={NewTrainingComponent}
              screenOptions={{
                headerShown: false,
              }}
            />
            <Stack.Screen
              name="Exercise"
              component={ExerciseComponent}
              screenOptions={{
                headerShown: false,
              }}
            />
            <Stack.Screen
              name="Plan"
              component={PlanComponent}
              screenOptions={{
                headerShown: false,
              }}
            />
            <Stack.Screen
              name="Training"
              component={TrainingComponent}
              screenOptions={{
                headerShown: false,
              }}
            />
            <Stack.Screen
              name="TrainingExercise"
              component={TrainingExerciseComponent}
              screenOptions={{
                headerShown: false,
              }}
            />
          </Stack.Navigator>
        </ApplicationProvider>
      </NavigationContainer>
      <Toast />
    </>
  );
};

const styles = StyleSheet.create({
  mainButton: {
    width: '40%',
    height: '40%',
    margin: 5,
    justifyContent: 'center',
  },
  mainView: {
    height: '60%',
    padding: 10,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'center',
  },
  mainImage: {
    width: 240,
    height: 220,
    marginTop: 40,
  },
  centralView: {
    height: '100%',
    width: '100%',
    backgroundColor: colorPalette.backgroundColor,
    alignItems: 'center',
  },
});

export default App;
