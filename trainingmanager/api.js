import AsyncStorage from '@react-native-async-storage/async-storage';
import Toast from 'react-native-toast-message';

export const saveData = async (name, value) => {
  try {
    const jsonValue = JSON.stringify(value);
    await AsyncStorage.setItem(`@${name}`, jsonValue);
    Toast.show({
      type: 'success',
      text1: 'Zapisano poprawnie',
    });
  } catch (e) {
    console.log(`${name} SAVE ERROR: `, e);
    Toast.show({
      type: 'error',
      text1: 'Błąd podczas zapisywania',
    });
  }
};

export const getData = async name => {
  try {
    const jsonValue = await AsyncStorage.getItem(`@${name}`);
    return jsonValue != null ? JSON.parse(jsonValue) : null;
  } catch (e) {
    console.log(`${name} GET ERROR: `, e);
  }
};
